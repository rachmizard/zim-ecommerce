// insert many query
db.products.insertMany([
    {

        namaProduk: 'Asus',
        kategori: 'laptop',
        harga: 7000000,
        kuantitas: 8
    },
    {

        namaProduk: 'Iphone XR',
        kategori: 'hp',
        harga: 9000000,
        kuantitas: 2
    },
    {

        namaProduk: 'Iphone 11 Pro Max',
        kategori: 'hp',
        harga: 17000000,
        kuantitas: 10
    }
])

// query find
db.products.find({
    harga: {
        $lt: 10000000
    },
    kategori: {
        $eq: 'hp'
    }
})

// query logical operator
db.products.find({
    $and: [
        {
            harga: {
                $gt: 10000000,
                $lt: 20000000
            }
        },
        {
            kategori: {
                $eq: 'hp'
            }
        }
    ]
})

//query logical & element operator
db.products.find({
    kategori: {
        $exists: true
    }
})
