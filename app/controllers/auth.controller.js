const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const CustomerService = require('../services/auth.service');
require('dotenv').config()

exports.login = async (req, res) => {
    const { email, password } = req.body;
    await CustomerService.findUser(email).then(user => {
        if (!user) {
            res.status(401).json({ message: 'Email/user tidak di temukan', type: 'email' });
        } else {
            const passwordIsValid = bcrypt.compareSync(password, user.password);

            if (passwordIsValid) { // apabila data password sama dengan user password
                const token = jwt.sign(user.toJSON(), process.env.ACCESS_TOKEN_SECRET, { expiresIn: 60 * 60 });

                res.json({ message: 'Berhasil login', user: req.decoded, token: token });
            } else { // apabila salah password
                res.status(401).json({ message: 'Password salah', type: 'password' });
            }
        }
    }).catch(err => {
        res.status(500).send({ message: err.message })
    });
}

exports.register = async (req, res) => {
    const body = req.body;
    const hashedPassword = bcrypt.hashSync(body.password, 8);
    body.password = hashedPassword;
    await CustomerService.createCustomer(body).then(user => {
        const token = jwt.sign(user.toJSON(), process.env.ACCESS_TOKEN_SECRET, { expiresIn: 60 * 60 });

        res.json({ message: 'Berhasil Mendaftar', user: req.decoded, token: token });
    }).catch(err => {
        res.status(500).send({ message: err.message })
    });
}

exports.authMe = async (req, res) => {
    const token = req.headers['x-access-token'];

    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, async function (err, decoded) {

        if (err) return res.status(401).send({ auth: false, message: err.message });

        const user = await CustomerService.getAuth(decoded._id);

        res.status(200).send(user);
    })
}


exports.updateProfile = async (req, res) => {
    const body = req.body;
    const token = req.headers['x-access-token'];

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, async function (err, decoded) {

        if (err) return res.status(401).send({ auth: false, message: err.message });

        await CustomerService.updateAuthUser(decoded._id, body).then(response => {
            res.status(200).send(response);
        }).catch(err => {
            res.status(500).send({ message: err.message })
        });
    })
}