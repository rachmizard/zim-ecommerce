const ProductService = require('./../services/product.service');

exports.getProducts = async (req, res) => {
    await ProductService.getProducts(req)
        .then(response => res.json(response))
        .catch(err => {
            res.status(400).json({ status: 400, message: err.message })
        });
}

exports.storeProduct = async (req, res) => {
    await ProductService.storeProduct(req)
        .then(response => res.json(response))
        .catch(err => res.status(400).json({ status: 400, message: err.message }))
}

exports.detailProduct = async (req, res) => {
    await ProductService.detailProduct(req)
        .then(response => res.json(response))
        .catch(error => {
            res.status(500).json({ error: error.message });
        })
}

exports.updateProduct = async (req, res) => {
    await ProductService.updateProduct(req)
        .then(response => res.json(response))
        .catch(err => res.status(500).json({ error: err.message }));
}

exports.postRating = async (req, res) => {
    await ProductService.postRating(req)
        .then(response => res.json(response))
        .catch(err => res.status(500).json({ error: err.message }));
}

exports.ratingProductDetail = async (req, res) => {
    await ProductService.ratingProductDetail(req)
        .then(response => res.json(response))
        .catch(err => res.status(500).json({ error: err.message }));
}

exports.reviewers = async (req, res) => {
    await ProductService.reviewersList(req)
        .then(response => res.json(response))
        .catch(err => res.status(500).json({ error: err.message }));
}