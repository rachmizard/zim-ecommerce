const OrderService = require('./../services/order.service');

exports.orderProduct = async (req, res, next) => {
    await OrderService.orderProduct(req)
        .then(response => {
            res.status(200).json(response)
        })
        .catch(err => {
            res.status(400).json({ status: 400, message: err.message })
        })
}

exports.orderDetail = async (req, res, next) => {
    await OrderService.orderDetail(req)
        .then(response => {
            res.status(200).json(response)
        })
        .catch(err => {
            res.status(400).json({ status: 400, message: err.message })
        })
}

exports.cancelOrder = async (req, res, next) => {
    await OrderService.cancelOrder(req)
        .then(response => {
            res.status(200).json(response)
        })
        .catch(err => {
            res.status(400).json({ status: 400, message: err.message })
        })
}

exports.checkOutOrder = async (req, res, next) => {
    await OrderService.checkOutOrder(req)
        .then(response => {
            res.status(200).json(response)
        })
        .catch(err => {
            res.status(400).json({ status: 400, message: err.message })
        })
}