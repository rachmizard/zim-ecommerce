
const CustomerService = require('./../services/customer.service');

exports.getCustomers = async (req, res, next) => {
    const { page, limit, sort } = req.query;
    try {
        const query = {};
        await CustomerService.getCustomers(query, page, limit, sort)
            .then(response => {
                res.status(200).json(response);
            })
            .catch(err => {
                res.status(400).json(err.message);
            })
    } catch (error) {
        res.status(400).json({ status: 400, message: error.message })
    }
}

exports.createCustomer = async (req, res, next) => {
    try {
        await CustomerService.createCustomer(req)
            .then(response => {
                res.status(200).json(response);
            })
            .catch(err => {
                res.status(400).json(err.message);
            })
    } catch (error) {
        res.status(400).json({ status: 400, message: error.message })
    }
}

exports.detailCustomer = async (req, res, next) => {
    try {
        await CustomerService.detailCustomer(req)
            .then(response => res.json(response))
            .catch(err => res.status(400).json(err));
    } catch (error) {
        res.status(400).json({ status: 400, message: error.message })
    }

}

exports.customerOrder = async (req, res, next) => {
    try {
        await CustomerService.customerOrder(req)
                .then(response => res.json(response))
                .catch(err => res.status(400).json(err));
    } catch (error) {
        res.status(400).json({ status: 400, message: error.message })
    }
}