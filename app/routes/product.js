const router = require('express').Router();
const ProductController = require('./../controllers/product.controller');
const isAuthenticated = require('./../middleware/isAuthenticated');


router.get("/products", ProductController.getProducts);

router.post("/products", ProductController.storeProduct);

router.get("/products/:id", ProductController.detailProduct);

router.post("/products/rating", isAuthenticated, ProductController.postRating);

router.get("/products/:id/rating", ProductController.ratingProductDetail);

router.get("/products/:id/reviewers", ProductController.reviewers);

router.patch("/products/:id", ProductController.updateProduct);

module.exports = router;