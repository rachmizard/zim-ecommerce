const router = require('express').Router();
const CustomerController = require('./../controllers/customer.controller');
const isAuthenticated = require('./../middleware/isAuthenticated');

router.get("/customers", CustomerController.getCustomers);

router.post("/customers", CustomerController.createCustomer);

router.patch("/customers/:id", CustomerController.detailCustomer);

router.get("/customers/:customerId/my-order", isAuthenticated, CustomerController.customerOrder);

module.exports = router;