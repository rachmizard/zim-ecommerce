
const customers = require('./customer');
const products = require('./product');
const orders = require('./order');
const auth = require('./auth');

module.exports = {
    customers,
    products,
    orders,
    auth
}