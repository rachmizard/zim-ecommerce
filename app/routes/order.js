const router = require('express').Router();
const OrderController = require('./../controllers/order.controller');
const isAuthenticated = require('./../middleware/isAuthenticated');

router.post("/order", isAuthenticated, OrderController.orderProduct);

router.get("/order/:orderId/detail", OrderController.orderDetail);

router.post("/order/:orderId/checkout", isAuthenticated, OrderController.checkOutOrder);

router.post("/order/:orderId/cancel", isAuthenticated, OrderController.cancelOrder);

module.exports = router;