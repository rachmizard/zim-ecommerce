const router = require('express').Router();
const AuthController = require('./../controllers/auth.controller')
const isAuthenticated = require('./../middleware/isAuthenticated');


router.post("/login", AuthController.login);

router.post("/register", AuthController.register);

router.post("/auth/me", isAuthenticated, AuthController.authMe);

router.put("/auth/profile/update", isAuthenticated, AuthController.updateProfile);

module.exports = router;