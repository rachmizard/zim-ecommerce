const bcrypt = require('bcryptjs');
const Customer = require('./../models/Customer');
const Order = require('./../models/Order');

exports.getCustomers = async (query, page, limit, sort) => {
    try {
        return await Customer
            .paginate(query, {
                select: { orders: 0, password: 0 },
                page: page,
                limit: limit ? limit : 5,
                sort: {
                    nama: sort === 'true' ? 1 : -1
                }
            });
    } catch (error) {
        throw error.message || 'Something went wrong!'
    }
}

exports.createCustomer = async (payload) => {
    try {
        const body = payload.body;
        const hashedPassword = bcrypt.hashSync(body.password, 8);
        body.password = hashedPassword;
        return await Customer.create(body);
    } catch (error) {
        throw error || 'Something went wrong!'
    }
}

exports.detailCustomer = async (req) => {
    try {
        const body = req.body;
        return await Customer.findOneAndUpdate({ _id: req }, { $set: body }, { runValidators: true })
    } catch (error) {
        throw error || 'Something went wrong!';
    }
}

exports.customerOrder = async (req) => {
    try {
        const query = {
            customer: req.params.customerId,
        }
        const options = {
            populate: {
                path: 'product',
                model: 'products'
            },
            page: req.query.page ? req.query.page : 1,
            limit: req.query.limit ? req.query.limit : 10
        }

        let sortBy = '_id';

        switch (req.query.sortBy) {
            case 'created_at':
                sortBy = 'created_at'
                break;

            case 'total':
                sortBy = 'total'
                break;
        }

        if(parseInt(req.query.limit) === -1) {
            delete options.limit;
        }

        if (req.query.sort && req.query.sortBy) {
            Object.assign(options, {
                sort: {
                    [sortBy]: req.query.sort === 'true' ? -1 : 0
                }
            })
        }
        if (req.query.orderStatus) {
            Object.assign(query, {
                orderStatus: parseInt(req.query.orderStatus)
            })
        }
        if (req.query.isPaid) {
            Object.assign(query, { isPaid: req.query.isPaid === 'true' ? true : false })
        }
        return await Order.paginate(query, options);
    } catch (error) {
        throw error || 'Something went wrong!';
    }
}