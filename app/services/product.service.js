
const Product = require('./../models/Product');
const ProductRating = require('./../models/ProductRating');
const ObjectId = require('mongoose').Types.ObjectId

exports.getProducts = async (req) => {
    try {
        const { page, limit, sort, sortBy, hargaMin, hargaMax } = req.query;
        const query = {};
        const options = {
            page: page,
            limit: limit ? limit : 5
        }


        if (req.query.search) {
            Object.assign(query, {
                $text: {
                    $search: req.query.search
                }
            })
        }

        if (sort && sortBy) {
            Object.assign(options, {
                sort: {
                    [sortBy]: sort.toUpperCase() === 'ASC' ? -1 : 1
                }
            })
        }

        if (hargaMin && hargaMax) {
            const minMax = {
                harga: { $gt: parseInt(hargaMin), $lt: parseInt(hargaMax) }
            }
            Object.assign(query, minMax);
        }

        return await Product.paginate(query, options);
    } catch (error) {
        throw new Error(error.message)
    }
}

exports.storeProduct = async (req) => {
    try {
        const body = req.body;
        const payload = {
            namaProduk: body.namaProduk,
            harga: body.harga,
            kuantitas: body.kuantitas,
            kategori: body.kategori,
            tags: body.tags ? JSON.parse(body.tags) : [],
            fullDescription: body.fullDescription,
            shortDescription: body.shortDescription,
            productPhoto: body.productPhoto
        }
        return await Product.create(payload);
    } catch (error) {
        throw new Error(error.message)
    }
}

exports.detailProduct = async (req) => {
    const productId = req.params.id;
    try {
        return await Product.find({ _id: productId });
    } catch (error) {
        throw new Error(error.message)
    }
}

exports.updateProduct = async (req) => {
    try {
        const productId = req.params.id;
        const newPayload = req.body;

        const oldProduk = await Product.findById(productId);

        const loadPayload = {
            namaProduk: newPayload.namaProduk ? newPayload.namaProduk : oldProduk.namaProduk,
            harga: newPayload.harga ? newPayload.harga : oldProduk.harga,
            kuantitas: newPayload.kuantitas ? newPayload.kuantitas : oldProduk.kuantitas,
            kategori: newPayload.kategori ? newPayload.kategori : oldProduk.kategori,
            tags: newPayload.tags ? JSON.parse(newPayload.tags) : oldProduk.tags,
            fullDescription: newPayload.fullDescription ? newPayload.fullDescription : oldProduk.fullDescription,
            shortDescription: newPayload.shortDescription ? newPayload.shortDescription : oldProduk.shortDescription,
            productPhoto: newPayload.productPhoto ? newPayload.productPhoto : oldProduk.productPhoto
        }
        return await Product.updateOne({ _id: productId }, {
            $set: loadPayload,
        })
    } catch (error) {
        throw new Error(error.message)
    }
}

exports.postRating = async (req) => {
    try {
        return await ProductRating.create(req.body)
    } catch (error) {
        throw new Error(error.message)
    }
}

exports.ratingProductDetail = async (req) => {
    try {
        const productId = req.params.id
        const averageRating = await ProductRating.aggregate([
            {
                $match: {
                    "productId": ObjectId(productId)
                }
            }, {
                $group: {
                    _id: null,
                    rating: {
                        $avg: "$rating"
                    },
                    totalRating: {
                        $sum: 1
                    }
                }
            }]);
        return averageRating;
    } catch (error) {
        throw new Error(error.message);
    }
}

exports.reviewersList = async (req) => {
    try {
        const { page, limit, sort } = req.query;
        const query = {
            productId: req.params.id
        };
        const options = {
            page: page,
            limit: limit ? limit : 3,
            select: {
                productId: 0
            },
            populate: {
                path: 'customerId',
                model: 'customers',
                select: {
                    orders: 0
                }
            }
        }

        if (sort) {
            Object.assign(options, {
                sort: {
                    created_at: sort.toUpperCase() === 'DESC' ? 1 : -1
                }
            })
        }
        return await ProductRating.paginate(query, options)
    } catch (error) {
        throw new Error(error.message)
    }
}