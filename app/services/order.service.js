const Product = require('./../models/Product')
const Customer = require('./../models/Customer')
const Order = require('./../models/Order')

exports.orderProduct = async (req) => {
    try {
        const body = req.body;
        const findProductById = await Product.findById(req.body.product);
        const customer = await Customer.findById(req.body.customer);

        if (findProductById.kuantitas > 0) {

            if (body.kuantitas > findProductById.kuantitas) {
                throw new Error('Your quantity is greater than product quantity! Product is insufficient!')
            }

            const totalHarga =  (findProductById.harga * body.kuantitas);
            // const sisaKuantitas = findProductById.kuantitas - body.kuantitas;

            const order = await Order.create({
                product: findProductById,
                customer: customer,
                kuantitas: body.kuantitas,
                total: totalHarga,
            })

            customer.orders.push(order)
            // findProductById.kuantitas = sisaKuantitas;

            await customer.save();
            await findProductById.save();

            return order;
        } else {
            throw new Error('The product is out of stock right now!');
        }
    } catch (error) {
        throw new Error(error)
    }
}

exports.orderDetail = async (req) => {
    const orderId = req.params.orderId
    const order = await Order.findById(orderId).populate({
        path: 'customer',
        model: 'customers',
        options: { select: '-orders' }
    })
        .populate({
            path: 'product',
            model: 'products'
        })
    if (!order) {
        throw new Error('Order not found!');
    }
    return order;
}


exports.checkOutOrder = async (req) => {
    try {
        const orderId = req.params.orderId
        const order = await Order.findById(orderId);
        // const findProductById = await Product.findById(order.product);
        // const sisaKuantitas = findProductById.kuantitas - order.kuantitas;

        if (order.orderStatus === 0) {
            if (order.isPaid) {
                throw new Error('The order has been paid, you are not be able to checkout back!');
            }
            if (order.orderStatus === 2) {
                throw new Error('The order is in progress, you are not be able to checkout back!');
            }
            if (order.orderStatus === 3) {
                throw new Error('The order has been delivered!');
            }
            order.isCheckout = true;
            order.orderStatus = 1;
            // console.log(sisaKuantitas)
            // findProductById.kuantitas = sisaKuantitas;
            await order.save()
            // await findProductById.save();
            return order;
        }
        throw new Error('You have been already checkout this order!');
    } catch (error) {
        throw new Error(error.message);
    }
}

exports.cancelOrder = async (req) => {
    try {
        const orderId = req.params.orderId
        const order = await Order.findById(orderId);
        if (order) {
            if (order.orderStatus !== 4) {
                if (order.isPaid) {
                    throw new Error('The order has been paid, you are not be able to cancel it!');
                }
                if (order.orderStatus === 2) {
                    throw new Error('The order is in progress, you are not be able to cancel it!');
                }
                if (order.orderStatus === 3) {
                    throw new Error('The order has been delivered!');
                }
                order.orderStatus = 4;
                await order.save()
                return order;
            }
            throw new Error('You have been canceled this order!');
        }
        throw new Error('Order not found!');
    } catch (error) {
        throw new Error(error.message);
    }
}