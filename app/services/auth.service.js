
const Customer = require('../models/Customer');
const ObjectId = require('mongoose').Types.ObjectId;

exports.findUser = async (email) => {
    return await Customer.findOne({ email: email })
}

exports.createCustomer = async (payload) => {
    return await Customer.findOne({ email: payload.email }).then(user => {
        if (user) {
            throw new Error('Email is already exists!');
        }
        return Customer.create(payload);
    })
}

exports.getAuth = async (id) => {
    return await Customer.findById(ObjectId(id))
}

exports.updateAuthUser = async (id, payload) => {
    return await Customer.findOne({ _id: ObjectId(id) }).then(async () => {
        const { email } = payload;
        if (email) {
            await Customer.findOne({ email: email }).then(res => {
                if (res) {
                    throw new Error('Email is already exists!');
                }
            });
        }
        return await Customer.updateOne({ _id: id }, payload);
    })
}

