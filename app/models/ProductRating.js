const mongoose = require('mongoose')

const mongoosePaginate = require('mongoose-paginate-v2');

const products_rating = mongoose.Schema({
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'product'
    },
    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'customer'
    },
    rating: {
        type: Number,
    },
    review: {
        type: String
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })


products_rating.plugin(mongoosePaginate);

module.exports = mongoose.model("products_ratings", products_rating);