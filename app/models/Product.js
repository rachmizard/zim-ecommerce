const mongoose = require('mongoose')

const mongoosePaginate = require('mongoose-paginate-v2');

const products = mongoose.Schema({
    namaProduk: {
        type: String,
        required: [true, 'Nama Produk harus di isi!'],
    },
    harga: {
        type: mongoose.Schema.Types.Number,
        required: [true, 'Harga harus di isi!']
    },
    kategori: {
        type: String,
        required: [true, 'Kategori harus di isi!']
    },
    kuantitas: {
        type: String,
        required: [true, 'Kuantitas harus di isi!']
    },
    tags: {
        type: Array,
        default: []
    },
    fullDescription: {
        type: String
    },
    shortDescription: {
        type: String
    },
    productPhoto: {
        type: String
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })
products.plugin(mongoosePaginate)
module.exports = mongoose.model("products", products);