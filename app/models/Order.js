const mongoose = require('mongoose')

const mongoosePaginate = require('mongoose-paginate-v2');

const orders = mongoose.Schema({
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'product'
    },
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'customer'
    },
    kuantitas: {
        type: Number,
        default: 0
    },
    total: {
        type: Number,
        default: 0
    },
    isCheckout: {
        type: Boolean,
        default: false
    },
    isPaid: {
        type: Boolean,
        default: false
    },
    orderStatus: {
        type: Number,
        default: 0
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })

orders.plugin(mongoosePaginate);

module.exports = mongoose.model("orders", orders);