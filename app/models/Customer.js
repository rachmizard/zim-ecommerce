const mongoose = require('mongoose')

const mongoosePaginate = require('mongoose-paginate-v2');

const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

const customers = mongoose.Schema({
    nama: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        validate: [validateEmail, 'Email format is invalid!']
    },
    password: {
        type: String,
        required: true,
    },
    nomor: {
        type: String,
        default: null
    },
    orders: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'orders',
            default: []
        }
    ]
})

customers.plugin(mongoosePaginate);

module.exports = mongoose.model("customers", customers);