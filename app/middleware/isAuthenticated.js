
require('dotenv').config();
const jwt = require('jsonwebtoken');

module.exports = function(req, res, next){
    const token = req.body.token || req.query.token || req.headers.authorization || req.headers['x-access-token']; // mengambil token di antara request
    if (token) { // jika ada token
      jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, function (err, decoded) { // jwt melakukan verify
        if (err) { // apa bila ada error
          res.status(401).send({ message: 'Failed to authenticate token' }); // jwt melakukan respon
        } else { // apa bila tidak error
          req.decoded = decoded; // menyimpan decoded ke req.decoded
          next(); // melajutkan proses
        }
      });
    } else { // apa bila tidak ada token
      return res.status(401).send({ message: 'No token provided.' }); // melkukan respon kalau token tidak ada
    }
  } 
