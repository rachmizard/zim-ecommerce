require('dotenv').config()

const bodyParser = require('body-parser')
const mongoose = require('mongoose');

const cors = require('cors');

// koneksi Ke Database
mongoose.connect(process.env.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true }).catch(err => {
    process.exit();
});

const indexRouter = require('./app/routes');

const express = require('express');

const app = express().disable('x-powered-by');

app.use(cors());

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

app.use(indexRouter.customers);
app.use(indexRouter.products);
app.use(indexRouter.orders);
app.use(indexRouter.auth)

app.listen(process.env.PORT, console.log('Server listening on ' + process.env.PORT + '....'));